import Vue from 'vue'
import App from './App.vue'
import VCalendar from 'v-calendar';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css'

Vue.use(VCalendar, {
    locale: "es",
    mask: {
        input: ['L', 'DD/MM/YYYY']
    }

})

Vue.config.productionTip = false

new Vue({
    render: h => h(App),
}).$mount('#app')