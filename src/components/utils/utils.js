const now = () => {
    let current = Date.now()
    let last = now.last || current
    now.last = current > last ? current : last + 1
    return now.last
};

const unique = (prefix = '', suffix = '') => {
    return prefix + now().toString(16) + suffix
}

const getDateFromString = (date, character = '-') => {
    let [day, month, year] = date.split(character);
    day = Number(day);
    month = Number(month);
    year = Number(year);
    month--;


    return new Date(year, month, day);
}


const hasErrorsForm = (form) => {
    let errors = 0;
    debugger;
    Object.keys(form).some(field => {
        if (Array.isArray(form[field])) {
            for (let i = 0; i < form[field].length; i++) {
                errors += hasErrorsForm(form[field][i]);
            }
        } else {
            if (form[field].hasOwnProperty('isError')) {
                if (form[field].isError) {
                    errors++;
                }
            }
        }
    })

    return errors;
}

const setErroresFormEsquemas = (form) => {
    let campos = ["fechaInicio", "fechaFin"];
    debugger;
    Object.keys(form).some(field => {
        if (Array.isArray(form[field])) {
            for (let i = 0; i < form[field].length; i++) {
                setErroresFormEsquemas(form[field][i]);
            }
        } else {
            if (form[field].hasOwnProperty('value')) {
                if (!form[field].value) {
                    form[field].isError = true;
                    form[field].message = "El campo es obligatorio";
                } else {
                    if (field == "fechaInicio") {
                        if (!isDateValid(form[field].value)) {
                            form[field].isError = true;
                            form[field].message = "Ingresa una fecha valida.";
                        } else {
                            if (!isDateValid(form['fechaFin'].value)) {
                                form['fechaFin'].isError = true;
                                form['fechaFin'].message = "Ingresa una fecha valida.";
                            } else {
                                if (getDateFromString(form[field].value, "/") > getDateFromString(form['fechaFin'], "/")) {
                                    form['fechaFin'].isError = true;
                                    form['fechaFin'].message = "La fecha fin debe ser mayor a fecha inicio.";

                                }

                            }
                        }
                    }
                    if (campos == "fechFin") {
                        if (!isDateValid(form[field].value)) {
                            this.form[field].isError = true;
                            this.form[field].message = "Ingresa una fecha valida.";
                        }
                    }
                }
            }
            // if (form[field].hasOwnProperty('funcion')) {
            //     if (!form[field].value) {
            //         this.form[field].isError = true;
            //         this.form[field].message = "El campo es obligatorio";
            //     }
            // }
            // if (form[field].hasOwnProperty('fechaInicio')) {
            //     if (!form[field].value) {
            //         this.form[field].isError = true;
            //         this.form[field].message = "El campo es obligatorio";
            //     }
            // }

            // if (form[field].hasOwnProperty('fechaFin')) {
            //     if (!form[field].value) {
            //         this.form[field].isError = true;
            //         this.form[field].message = "El campo es obligatorio";
            //     }
            // }
        }
    })
}

const isDateValid = (dateString) => {
    let isValid = false;

    if (dateString.length != 8)
        return isValid;

    let regexExpresion = new RegExp(/^(((0[1-9]|[12][0-9]|3[01])[- /.](0[13578]|1[02])|(0[1-9]|[12][0-9]|30)[- /.](0[469]|11)|(0[1-9]|1\d|2[0-8])[- /.]02)[- /.]\d{4}|29[- /.]02[- /.](\d{2}(0[48]|[2468][048]|[13579][26])|([02468][048]|[1359][26])00))$/);

    isValid = regexExpresion.test(dateString);

    return isValid;
}

export {
    unique,
    getDateFromString,
    hasErrorsForm,
    setErroresFormEsquemas
};